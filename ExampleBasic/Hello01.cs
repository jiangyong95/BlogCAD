﻿using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.Runtime;

[assembly: CommandClass(typeof(ExampleBasic.Hello01))]
namespace ExampleBasic
{
    public class Hello01
    {
        /// <summary>
        /// CommandMethod特性中的第一个参数'globalName'，表示CAD反射读取该特性时，命令行能够显示和执行的名称。
        /// </summary>
        [CommandMethod("hello_method")]
        public void MyFunction()
        {
            Application.ShowAlertDialog("Hello World!!!");
        }
    }
}
