﻿using Autodesk.AutoCAD.ApplicationServices.Core;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;

[assembly: CommandClass(typeof(ExampleBasic.Create02))]
namespace ExampleBasic
{
    public class Create02
    {
        /// <summary>
        /// CommandMethod特性中的第一个参数'globalName'，表示CAD反射读取该特性时，命令行能够显示和执行的名称。
        /// </summary>
        [CommandMethod("create_method")]
        public void MyFunction()
        {
            //由静态对象Application获取文档对象
            var doc = Application.DocumentManager.MdiActiveDocument;

            //模态窗体，调用此函数时，需要锁定文档
            using (doc.LockDocument())
            {
                //当前文档的数据库对象
                var db = doc.Database;

                //开启事务
                using (var host = doc.TransactionManager.StartTransaction())
                {
                    //获取块表
                    using (var blockTable = host.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable)
                    {
                        //获取快表中的块表记录（模型空间）
                        using (var modelspaceRecord = host.GetObject(blockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord)
                        {
                            //输入参数
                            var origin = new Point3d(10, 10, 0);
                            var normal = Vector3d.ZAxis;
                            var radius = 100;

                            //调用构造函数来创建圆
                            var cirlce = new Circle(origin, normal, radius);

                            //块表记录，添加该实体
                            var circleId = modelspaceRecord.AppendEntity(cirlce);

                            //事务，记录该实体
                            host.AddNewlyCreatedDBObject(cirlce, true);
                        }
                    }

                    host.Commit();
                }
            }
        }
    }
}
